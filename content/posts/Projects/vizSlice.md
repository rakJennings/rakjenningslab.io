---
title: vizSlice
description: vizSlice utilizes the D3 JavaScript library to create several interactive visualizations of program slicing.
date: 2016-04-27T00:00:00
menu:
  sidebar:
    name: vizSlice
    identifier: vizslice
    parent: projects
    weight: 1
tags: ["vizSlice", "program slicing", "visualization", "debugging", "software engineering"]
categories: ["projects"]
hero: "/images/posts/vizSlice.jpg"
---

vizSlice utilizes the D3 JavaScript library to create several interactive visualizations of program slicing.

[See the source](https://gitlab.com/rakJennings/vizSlice)