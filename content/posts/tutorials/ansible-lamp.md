---
title: Setting Up a LAMP Server
description: Tutorial using Ansible 2.7 to set up a LAMP server.
date: 2018-10-15
menu:
  sidebar:
    name: Setting Up a LAMP Server
    identifier: ansible-lamp
    parent: tutorials
    weight: 1
tags: ["tutorial", "ansible"]
categories: ["tutorial"]
---

{{< alert type="warning" >}}
This tutorial was written for Ansible 2.7 and some of the processes may have changed with more recent Ansible versions.
{{< /alert >}}

## Writing The Playbook

### Setup

1. Install [Ansible](https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html)

2. Create a folder for our ansible project: `ansible_playground`.  All of the remaining commands should take place inside this folder unless otherwise mentioned.

3. Create a host file (and the associated folder) named `ansible_inventory/hosts.yml`. Here is the example Host file format.  If your remote machine doesn't use python2 by default, you'll need to set the variable `ansible_python_interpreter` to `/usr/bin/python3`.
    ```yaml
    all:
      children:
        server-group-00:
          hosts:
            server-name-00:
              # These variables apply only to server-name-00
              ansible_ssh_host: <ip>
            server-name-01:
              # These variables apply only to server-name-01
              ansible_ssh_host: <ip>
          vars:
            # These variables apply to all servers in server-group-00
            ansible_user: <server login user>
        server-group-01:
          hosts:
            server-name-02:
              ansible_ssh_host: <ip>
            server-name-03:
              ansible_ssh_host: <ip>
          vars:
            # These variables apply to all servers in server-group-01
      vars:
        # These variables apply to all servers
    ```
    For now you just need 1 server group and 1 server.
    You'll need your server ip and server username.

4. Create an Ansible config file.  Named `ansible.cfg`.

5. Add the following to the cfg file.

    ```cfg
    [defaults]
    inventory = ansible_inventory
    roles_path = roles
    host_key_checking = false
    ```

    This sets the locations of the roles and inventory to local folders.

### Your First Playbook

Time to create our first playbook.

1. Create a file called `example.yml`. Add the following code:
    ```yml
    hosts: all # run this playbook on all hosts in inventory file
    become: no  # don't run as sudo by default
    become_method: sudo # when using 'become' use sudo
    ```
    This forms the basis of you first playbook file.  We are telling it to run on all hosts, not to become `sudo` for every action but also to use `sudo` as our become method when we need to assume a role.

2. We're going to setup a LAMP server.  To do this we're going to use Ansible roles from Ansible Galaxy.
    - Go to [Ansible Galaxy](https://galaxy.ansible.com)
    - Find geerlingguy's apache, mysql, and php roles and install them.
    - When done, you should see them in your `roles` folder.

3. Now that the roles are installed, we need to utilize them in the script.
    - Add the following to the `example.yml` file. Be sure to fix the indentation.
    
    ```yml
    roles:
      - role: geerlingguy.mysql
        become: yes
      - role: geerlingguy.apache
        become: yes
        vars:
          apache_packages:
            - apache2
            - apache2-utils
            - apache2-dev
      - role: geerlingguy.php
        become: yes
    ```
    
    - We need to add a few variables to `hosts.yml` so that MySQL gets setup correctly. Add a variable `mysql_root_password: <pass>` and provide a secure password.
    - After we've done this, we'll need to secure the hosts file since it now contains secure information.  Run `ansible-vault encrypt path/to/hosts.yml`.  This will encrypt the file using the password that you provide. Now that your hosts file is encrypted, you'll need to use special commands to view, edit, or otherwise handle the file.  View `ansible-vault` documentation [here](https://docs.ansible.com/ansible/2.6/user_guide/vault.html).

## Running the Script

To run the script, simply type `ansible-playbook example.yml -k -K --ask-vault-pass` into the command line from the project directory on your local machine.  The `-k` flag will ask for your connection password.  The `-K` flag will ask for your sudo password.  See Ansible's documentation for more information. Finally, `--ask-vault-pass` will ask for your vault password so it can decrypt the hosts file. After you run the script you should have a LAMP server ready to go.  You should double check you MySQL, Apache, and PHP installs manually to ensure the script you've written works correctly. 

## Tips, Tricks, and Additional Info

- YML files don't use tabs! Make sure you're using spaces and consistent indentations.  Tabs will break your YML files.

- ALL commands should be run in the `ansible-playground` directory.  

- Additional documentation can be found on Ansible's website [here](https://docs.ansible.com/). Ansible is capable of many things, including server configuration, application deployment, and user creation.

- Ansible acts like its own documentation.  If you need to understand you stack, Ansible scripts are easy to read and modify.

- Additional example playbooks can be found [here](https://github.com/ansible/ansible-examples).

- For some more information about how Ansible works go [here](https://www.ansible.com/overview/how-ansible-works).


This tutorial can also be found [here](https://gitlab.com/tutorials-notes/Ansible).
