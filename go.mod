module gitlab.com/rakJennings/rakjennings.gitlab.io

go 1.20

require (
	github.com/foo-dogsquared/hugo-mod-simple-icons v0.0.0-20240424231958-19892d58d5db // indirect
	github.com/hugo-toha/toha/v4 v4.4.0 // indirect
)
